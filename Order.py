import pandas as pd

coffee_bar_df = pd.read_csv('Coffeebar_2013-2017.csv', sep=';')
coffee_bar_df['FOOD'] = coffee_bar_df['FOOD'].fillna('nothing')


class Order(object): #creating a class with the orders
    order_date = ''
    customer_id = ''
    drink = ''
    food = ''
    amount = 0

    def __init__(self, order_date, customer_id, drink, food): #attributes that need to be set up
        self.order_date = order_date
        self.customer_id = customer_id
        self.drink = drink
        self.food = food
        self.amount = self.calculate_amount()

    def get_order_date(self): #use of "setters" and "getters" 
        return self.order_date

    def set_order_date(self, order_date):
        self.order_date = order_date

    def get_customer_id(self):
        return self.customer_id

    def set_customer_id(self, customer_id):
        self.customer_id = customer_id

    def get_drink(self):
        return self.drink

    def set_drink(self, drink):
        self.drink = drink

    def get_food(self):
        return self.food

    def set_food(self, food):
        self.food = food

    def get_amount(self):
        return self.amount

    def set_amount(self, amount):
        self.amount = amount

    def calculate_amount(self):
        self.amount = get_food_price(self.food) + get_drink_price(self.drink)
        return self.amount
#protecting the code from any accidental changes

def get_food_price(food):  #function giving prices for foods
    if food in coffee_bar_df.FOOD.unique():
        if food == "sandwich":
            price = 5
        elif food == "cookie":
            price = 2
        elif food == "nothing":
            price = 0
        else:
            price = 3
    else:
        price = 0
    return price


def get_drink_price(drink): #function giving prices for drinks
    drink = drink
    if drink in coffee_bar_df.DRINKS.unique():
        if drink == "milkshake":
            price = 5
        elif drink == "frappucino":
            price = 4
        elif drink == "water":
            price = 2
        else:
            price = 3
    else:
        price = 0
    return price