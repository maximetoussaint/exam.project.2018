# Exam.Project.2018
"""Welcome to our coffeebar simulation. We're Maxime and Mehdi and we worked on this project for our course in Data
analytics. The final goal of this project is to simulate five years of data for our virtual coffeebar. Therefore we
created customers, orders and realised some additional plots for a better visual representation. In this readme, we
will inform you about the content of our work."""



#INDEX : (Four files must be run in this order: "Exploratory.py, Order.py, Customer.py, Simulation.py"

#1) Analysis of our dataset "Coffeebar_2013-2017.csv"
#2) Creation of our classes for the simulation
#3) Simulation
#4) Additional questions


#1) Analysis of our dataset "Coffeebar_2013-2017.csv" :

In our Python File "Exploratory.py", we made a dataframe from our dataset to facilitate our anylisis.
We determined the average that a customer buys a certain type of food or drink at any given time ("#question3").
We will use these probabilities to create customers who buy following these rules.


#2) Creation of our classes for the simulation :

This part concerns the Python Files "Order.py" and "Customer.py".
In "Order", we created a class with all the attributes for an order (order date, customer_id, drink, ...). We added
functions to determine the prices of foods and drinks.
In "Customer", we created a class with all the attributes for a customer (customer id, customer type,...). Then we
created two subclasses for the two types of customers, the ones that only come once and the returning customers.


#3) Simulation :

This part concerns the Python Files "Simulation.py".
We defined the numbers of returning customers and included the probability of having a returning or only once customer.
We created the minimum budget for a customer to be able to buy the most expensive food and drink.
Then we made the simulation for one day and we had to take into account that returning customers have their budget
decreased.
We finally realised the simulation for the five years and added the necessary plots.


#4) Additional questions :

In this part, we were asked, in short, to showcase how easy it is to make changes. 
What would happen if we lower the returning customers to 50? And if the prices change from the beginning of 2015 and go up 
by 20%? And if the budget of hipsters drops to 40? We answered all these questions by adding "# part 4" in different parts of our 
code and then by explaining the changes that need to be done to adapt the code for those additional questions. 
Note that the script "Order.py" is the only one that doesn't require any adaptations to fulfill the guidelines of part 4. 
The scripts "Exploratory.py", "Customer.py" and "Simulation.py" were edited with necessary comments to adapt for part 4. 
We didn't run the simulation again for this part because of a lack of time, but we showed we were able to do adapt the code 
for a possible simulation. 

