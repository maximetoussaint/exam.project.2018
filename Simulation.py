from random import randint
import pandas as pd
from datetime import timedelta, date
import matplotlib.pyplot as plt
import numpy as np

import Customer


def add_1000_returning_customer_in_customer_list(): # part 4: 1000 has to become 50 
    #we have 1000 "returning" customers
    for customer_id in range(1000): 
        if randint(1, 100) >= 33:
            Customer.add_customer(customer_id, 'Returning', 'regular')
        else:
            Customer.add_customer(customer_id, 'Returning', 'hipster')


def get_customer():
    global customer
    #20% to have a "returning" customer coming => we pick it up in the created list
    
    # part 4 / adjust at least one thing you would like to see the impact of :
    #20 has to become 10 to adapt the code to our personal question  
    
    if randint(1, 100) <= 20:
        customer = Customer.customer_list[randint(0, 999)]
        #the customer needs a minimum budget of 10euro to buy the most expensive food and drink
        #if the budget is lower than 10, we pick up a new customer
        if customer.get_budget() < 10:
            get_customer()
    #if not, we have an "onlyonce" customer
    else:
        #we have 1 chance on ten that the customer type is "tripadvisor"
        if randint(1, 10) <= 1:
            customer = Customer.CustomerOnlyOnce('tripadvisor', len(Customer.customer_list))
            Customer.add_customer(len(Customer.customer_list), 'OnlyOnce', 'tripadvisor')
        else:
            customer = Customer.CustomerOnlyOnce('onetime', len(Customer.customer_list))
            Customer.add_customer(len(Customer.customer_list), 'OnlyOnce', 'onetime')
    return customer


def one_day_simulation(day):

    add_1000_returning_customer_in_customer_list()

    distinct_hour = Customer.Exploratory.coffee_bar_df['HOUR'].unique()
    simulation_result = {'TIME': [], 'CUSTOMER': [], 'DRINKS': [], 'FOOD': [], 'AMOUNT': []}

    #simulation for one day
    for hour in distinct_hour:
        #we pick up the current customer
        actual_customer = get_customer()

        #we pick up purchases and create an order
        customer_buys = Customer.customer_probability_buys(hour)
        order = Customer.Order.Order(day + " " + hour, actual_customer.get_customer_id(),
                                     customer_buys[0], customer_buys[1])

        # part 4 : we increase the prices (20%) from 2015
        # here is the code to realise it:
        # amount = order.get_amount()
        # if datetime.strptime(day, '%Y-%m-%d').year >= 2015:
        # amount = order.get_amount() + order.get_amount() * 0.2

        #if it's a returning customer, we have to decrease his budget
        if actual_customer.get_customer_type() == 'Returning':
            actual_customer.set_budget(actual_customer.get_budget() - order.get_amount())

        print(order.get_order_date(), actual_customer.get_customer_id(), order.get_drink(),
              order.get_food(), order.get_amount())

        simulation_result['TIME'].append(order.get_order_date())
        simulation_result['CUSTOMER'].append(actual_customer.get_customer_id())
        simulation_result['DRINKS'].append(order.get_drink())
        simulation_result['FOOD'].append(order.get_food())
        simulation_result['AMOUNT'].append(order.get_amount())

    return pd.DataFrame(simulation_result)


# one_day_data_frame = one_day_simulation('2018-01-01')


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


def five_year_simulation(start_date, end_date):
    data_frame_by_day = []
    for single_date in date_range(start_date, end_date):
        data_frame_by_day.append(one_day_simulation(single_date.strftime("%Y-%m-%d")))

    return pd.concat(data_frame_by_day, ignore_index=True)


five_year_data_frame = five_year_simulation(date(2018, 1, 1), date(2023, 1, 1))
#we repace nothing per 'null' not to take it into account for the count for graphs
five_year_data_frame = five_year_data_frame.replace('nothing', np.nan)
five_year_data_frame.to_csv('data/Coffeebar_2018-2022.csv', sep=';', index=False)

# graphs
food_by_year = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).year).agg({'FOOD': 'count'})
food_by_month = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).month).agg({'FOOD': 'count'})
food_by_week = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).week).agg({'FOOD': 'count'})
food_by_day = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).day).agg({'FOOD': 'count'})
food_by_hour = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).hour).agg({'FOOD': 'count'})

drink_by_year = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).year).agg({'DRINKS': 'count'})
drink_by_month = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).month)\
    .agg({'DRINKS': 'count'})
drink_by_week = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).week).agg({'DRINKS': 'count'})
drink_by_day = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).day).agg({'DRINKS': 'count'})
drink_by_hour = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).hour).agg({'DRINKS': 'count'})

amount_by_year = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).year).sum()
amount_by_month = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).month).sum()
amount_by_week = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).week).sum()
amount_by_day = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).day).sum()
amount_by_hour = five_year_data_frame.groupby(pd.DatetimeIndex(five_year_data_frame.TIME).hour).sum()

ax = food_by_year[['FOOD']].plot(kind='bar', title="number of food by year",
                                 figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()

ax = food_by_month[['FOOD']].plot(kind='bar', title="number of food by month",
                                  figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Month", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()

ax = food_by_week[['FOOD']].plot(kind='bar', title="number of food by week",
                                 figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Week", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()

ax = food_by_day[['FOOD']].plot(kind='bar', title="number of food by day",
                                figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Day", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()

ax = food_by_hour[['FOOD']].plot(kind='bar', title="number of food by hour",
                                 figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()

ax = drink_by_year[['DRINKS']].plot(kind='bar', title="number of drinks by year",
                                    figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()

ax = drink_by_month[['DRINKS']].plot(kind='bar', title="number of drinks by month",
                                     figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Month", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()

ax = drink_by_week[['DRINKS']].plot(kind='bar', title="number of drinks by week",
                                    figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Week", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()

ax = drink_by_day[['DRINKS']].plot(kind='bar', title="number of drinks by day",
                                   figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Day", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()

ax = drink_by_hour[['DRINKS']].plot(kind='bar', title="number of drinks by hour",
                                    figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()

ax = amount_by_year[['AMOUNT']].plot(kind='bar', title="amount by year",
                                     figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("Amount", fontsize=12)
plt.show()

ax = amount_by_month[['AMOUNT']].plot(kind='bar', title="amount by month",
                                      figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Month", fontsize=12)
ax.set_ylabel("Amount", fontsize=12)
plt.show()

ax = amount_by_week[['AMOUNT']].plot(kind='bar', title="amount by week",
                                     figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Week", fontsize=12)
ax.set_ylabel("Amount", fontsize=12)
plt.show()

ax = amount_by_day[['AMOUNT']].plot(kind='bar', title="amount by day",
                                    figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Day", fontsize=12)
ax.set_ylabel("Amount", fontsize=12)
plt.show()

ax = amount_by_hour[['AMOUNT']].plot(kind='bar', title="amount by hour",
                                     figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Hour", fontsize=12)
ax.set_ylabel("Amount", fontsize=12)
plt.show()


#PART 4: CHANGES TO BE DONE IF WE WANT TO ANSWER QUESTIONS OF PART 4 

# if we lower the returning customers to a number of 50
# their budget will decrease faster because we will now pick up 50 instead of 1000 at first
# they will spend their budget faster and we’ll have more onetime customers 
# changes to adapt the code are done in part the function "add_1000_returning_customer_in_customer_list" with "#" 

# if the prices change from the beginning of 2015 and go up by 20%
# Returning customers’ budget will decrease faster and they will spend their budget faster 
# changes to adapt the code are done in part “simulation for one day” with “#” 

# if the budget of hipsters drops to 40
# idem before, they will spend their budget faster 
# changes to adapt the code are done in part 
# “CustomerReturning(Customer).set_budget” with “#” 


# adjust at least one thing you would like to see the impact of : the probability of 20% to get a returning customer decreases to 10%
# changes to adapt the code are done in the function “get_customer” with “#” 