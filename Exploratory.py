import pandas as pd
import matplotlib.pyplot as plt

coffee_bar_df = pd.read_csv('Coffeebar_2013-2017.csv', sep=';')

# part 4 : changes to show some buying histories of returning customers for your simulations
# print(coffee_bar_df.groupby(coffee_bar_df.CUSTOMER.tolist(), as_index=False).size())
# print(coffee_bar_df.duplicated(subset='CUSTOMER'))
# print(coffee_bar_df.CUSTOMER.unique().tolist())

# Question 1
print("Number of distinct customer : ", len(pd.Series.value_counts(coffee_bar_df.CUSTOMER))) #showing number of distinct customers
print("List of distinct food ", coffee_bar_df.FOOD.unique()) #showing the different kind of foods
print("List of distinct drink ", coffee_bar_df.DRINKS.unique()) #same for drinks

# Question 2
food_by_year = coffee_bar_df.groupby(pd.DatetimeIndex(coffee_bar_df.TIME).year).agg({'FOOD': 'count'}) #showing number of sold food per year

drink_by_year = coffee_bar_df.groupby(pd.DatetimeIndex(coffee_bar_df.TIME).year).agg({'DRINKS': 'count'}) #same for drinks

ax = food_by_year[['FOOD']].plot(kind='bar', title="number of food by year",
                                 figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("Food", fontsize=12)
plt.show()


ax = drink_by_year[['DRINKS']].plot(kind='bar', title="number of drinks by year",
                                    figsize=(10, 5), legend=True, fontsize=12)
ax.set_xlabel("Year", fontsize=12)
ax.set_ylabel("Drink", fontsize=12)
plt.show()
#creating two plots for previous dataframes

# Question 3
coffee_bar_df['HOUR'] = coffee_bar_df['TIME'].str[-8:]
#extracting hours from the time column in order to create a single hours column
coffee_bar_df['FOOD'] = coffee_bar_df['FOOD'].fillna('nothing')
# replace NaN by nothing for display



def customer_average_buys(hour):
    line_by_hour = coffee_bar_df[coffee_bar_df['HOUR'] == hour]
    total_customer_by_hour = line_by_hour.agg({'CUSTOMER': 'count'}) #counting number of customers per hours
    distinct_drink_by_hour = line_by_hour['DRINKS'].unique()
    distinct_food_by_hour = line_by_hour['FOOD'].unique()
    average_drink_by_hour = {} #done to use a dictionnary
    average_food_by_hour = {}  #done to use a dictionnary


    for drink in distinct_drink_by_hour: #loop for drinks
            total_drink = line_by_hour[line_by_hour['DRINKS'] == drink].agg({'DRINKS': 'count'})
            average_drink = float(round((float(total_drink)/total_customer_by_hour)*100, 2))
            average_drink_by_hour[drink] = average_drink

    for food in distinct_food_by_hour: #loop for foods
        total_food = line_by_hour[line_by_hour['FOOD'] == food].agg({'FOOD': 'count'})
        average_food = float(round((float(total_food) / total_customer_by_hour) * 100, 2)) #probability = total of drink or food sold / number of orders
        average_food_by_hour[food] = average_food

    return average_drink_by_hour, average_food_by_hour


distinctHour = coffee_bar_df['TIME'].str[-8:].unique() #showing the different hours

for time in distinctHour:
    print('average probabilities at ' + time)
    customer_average = customer_average_buys(time)
    print(customer_average[0], '\n')
    print(customer_average[1], '\n')

#loop to obtain the probability for all distinct hours