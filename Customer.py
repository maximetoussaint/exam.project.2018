from random import randint

import Exploratory
import Order

customer_list = []


class Customer(object): #creating a class with the customers
    customer_id = ''
    customer_type = ''
    budget = 0
    orders = [] #all the attributes for class "customer"

    def __init__(self, customer_id):
        self.customer_id = customer_id

    def get_customer_id(self):
        return self.customer_id

    def set_customer_id(self, customer_id):
        self.customer_id = customer_id

    def get_customer_type(self):
        return self.customer_type

    def set_customer_type(self, customer_type):
        self.customer_type = customer_type

    def get_budget(self):
        return self.budget

    def set_budget(self, budget):
        self.budget = budget

    def get_orders(self):
        return self.orders

    def set_orders(self, orders):
        self.orders = orders


class CustomerOnlyOnce(Customer): #creating a subclass
    budget = 100
    only_once_type = ''
    tip = 0
    customer_type = 'OnlyOnce'

    def __init__(self, only_once_type, customer_id):
        super().__init__(customer_id)
        self.only_once_type = only_once_type

    def get_only_once_type(self):
        return self.only_once_type

    def set_only_once_type(self, only_once_type):
        self.only_once_type = only_once_type

    def get_tip(self):
        if self.only_once_type == 'tripadvisor':
            self.tip = randint(1, 10)


class CustomerReturning(Customer): #creating a subclass
    returning_type = ''
    customer_type = 'Returning'

    def __init__(self, returning_type, customer_id):
        super().__init__(customer_id)
        self.returning_type = returning_type
        self.budget = self.set_budget(-1)

    def get_returning_type(self):
        return self.returning_type

    def set_returning_type(self, returning_type):
        self.returning_type = returning_type

    def set_budget(self, budget):
        if budget == -1:
            if self.returning_type == 'hipster':
                return 500 # part 4 : 500 has to become 40 to adapt the code
            else:
                return 250
        else:
            self.budget = budget


def add_customer(customer_id, customer_type, customer_subtype): #creating a customer to the subclass "CustomerOnlyOnce" or "CustomerReturning"
    if customer_type == 'OnlyOnce':
        customer_list.append(CustomerOnlyOnce(customer_subtype, customer_id))
    else:
        customer_list.append(CustomerReturning(customer_subtype, customer_id))


def add_order(customer_id, date, drink, food):
    customer_list[customer_id].get_orders.append(Order.Order(date, customer_id, drink, food))


def get_last_order(customer_id):
    customer = customer_list[customer_id]
    last_order_date = customer.get_orders().get_order_date()
    last_order = customer.get_orders()[0]
    for order in customer.get_orders():
        if order.get_order_date() > last_order_date:
            last_order_date = order.get_order_date()
            last_order = order

    return last_order


def customer_probability_buys(time):
    global drink_buy, food_buy
    customer_average = Exploratory.customer_average_buys(time)
    list_drink = customer_average[0].keys()
    drink_proba = {}
    temp_drink = 0
    for drink in list_drink:
        drink_proba[drink] = customer_average[0][drink] + temp_drink
        temp_drink += customer_average[0][drink]

    probability = randint(0, 100)

    for drink in drink_proba:
        if probability <= drink_proba[drink]:
            drink_buy = drink
            break

    list_food = customer_average[1].keys()
    food_proba = {}
    temp_food = 0
    for food in list_food:
        food_proba[food] = customer_average[1][food] + temp_food
        temp_food += customer_average[1][food]

    probability = randint(0, 100)

    for food in food_proba:
        if probability <= food_proba[food]:
            food_buy = food
            break

    return drink_buy, food_buy
